# miniProject6-sm957

### Requirements

Instrument a Rust Lambda Function with Logging and Tracing

- Add logging to a Rust Lambda function
- Integrate AWS X-Ray tracing
- Connect logs/traces to CloudWatch

### Step
- Add logging to a Rust Lambda function. Using tracing_subscriber::FmtSubscriber::builder() to create an FmtSubscriber.

```
let subscriber = tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();
tracing::subscriber::set_global_default(subscriber).expect("Fail to set default subscriber");
```
- Enable AWS X-Ray tracing under the Configuration section of lambda function.
- Add permissions for the IAM role, include _IAMFullAccess, AWSXrayFullAccess, AWSXRayDaemonWriteAccess_.
- Create a test event, then go to CloudWatch you can see the updated logging and tracing.


### Screenshots
- Enable AWS X-Ray tracing under the Configuration section
![](pic/img2.png)

- Screenshot of permissions for the IAM role
![](pic/img1.png)

- Create a test event
![](pic/img3.png)

- Updated logging and tracing in CloudWatch
![](pic/img4.png)

- Logs of the test event
![](pic/img5.png)




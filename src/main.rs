use serde::Deserialize;
use serde_json::{json, Value};
use aws_config::{load_from_env};
use aws_sdk_dynamodb::{Client, model::AttributeValue};
use lambda_runtime::{Error as LambdaError, LambdaEvent, service_fn};
use tracing::{Level};
use tracing_subscriber::FmtSubscriber;

#[derive(Deserialize)]
struct Request {
    id: String,
    name: String,
}

#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    let subscriber = tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();
    tracing::subscriber::set_global_default(subscriber).expect("Fail to set default subscriber");

    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}


async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    let request: Request = serde_json::from_value(event.payload)?;

    let config = load_from_env().await;
    let client = Client::new(&config);

    let name = post_new_info(&client, &request.id, &request.name).await?;
    Ok(json!({"name": name}))
}

async fn post_new_info(client: &Client, id: &str, name: &str) -> Result<(), LambdaError> {
    let table_name = "StuInfo"; 
    let id_av = AttributeValue::S(id.to_string());
    let name_av = AttributeValue::S(name.to_string());

    client.put_item()
        .table_name(table_name)
        .item("id", id_av)
        .item("name", name_av)
        .send()
        .await?;

    Ok(())
}